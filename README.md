# Home Automation

Home automation - Plant monitoring from anywhere in the world, plug and play.  

Before anything else, feel free to read the pdf file located in the root directory, to better gain a overview over how everything is set up and structured, and what the general purpose of this project is.

<img src="icon.png" alt="drawing" style="width:100px; border-radius:2%; margin-bottom:30px;"/>

## The Project
The project will consist of the main goal: To create a �plug-and-play� system that will be portable and intuitive to use. It will be a plant-observing and maintaining system, with monitoring capabilities, intended for use on small potted plants, whether it be small fruits and lettuce, or simply flowers. It will consist of two parts:

Measuring: Measure the soil contents (soil moisture, and nutrition if possible).

Monitoring: Monitoring the data received on a home base such as a tablet or cellphone, using node-RED as a platform for the user interface. It will be able to show the data received and alert the user if anything is out of the ordinary, or if the data is not within nominal values.

### Setup
The project is based on my own tri-planter-box which I have outside. A sensor is to be hung on each level to monitor them:

<img src="PlanterBoxes.jpg" alt="drawing" style="width:auto; border-radius:2%; margin-bottom:30px;"/>

### Structure
For everything essential, read the PDF file "Home Automation" located here in the root folder.  
It contains everything that there is need to know, and the material that *you* need to get started, can be found in three of the four folders, which also are located in the root directory of this project.

#### Code  
The code folder contains the primary code, but the final code files in python, can be found in the "planters" folder, for each level of the box.

#### Documentation 
This folder contains all relevant documentation of modules and the likes.

#### ProjectManagement  
For project management while working on the project, a track was kept in this folder.

<br>

If there is anything else, feel free to contact me on Jacobmail2@hotmail.com.