# --------------------------------------------------
#This script is for the thesis of 4th semester of IT-Technology, by Jacob Suurballe Petersen.
#Monitor plant health by monitoring soil moisture, and activating motor when below threshold.
# --------------------------------------------------

#imports
from machine import ADC, Pin, PWM #ADC for moisture sensor and PWM for motor
import time
import motor

#variables and pin assignments
soil = ADC(0)
maxM = 700
minM = 200
green = Pin(0, Pin.OUT) #D3
green.value(0) #default off
red = Pin(2, Pin.OUT) #D4
red.value(0) #default off
motor = PWM(Pin(15)) #D8

def motor_stop():
  motor = PWM(Pin(15))
  motor.deinit()

def watering():
  motor = PWM(Pin(15), freq=50, duty=150)
  time.sleep(0.25) #wait to turn half a rotation
  motor.deinit()
  time.sleep(2) #stay open for x seconds
  motor.freq(50)
  motor.duty(150)
  time.sleep(0.25) #wait to turn the other half and close
  motor.deinit()
  
def goodTimes(moisture):
  while(moisture>=50):
    red.value(0)
    green.value(1)
    moisture = (maxM-soil.read())*100/(maxM-minM) #re-read
    print(">> Soil moisture nominal, " + "%.2f" %moisture + "%")
    motor_stop()
    time.sleep(1)

def mainLoop():
  while True:
    moisture = (maxM-soil.read())*100/(maxM-minM)
    #print(">> moisture: " + "%.2f" % moisture + "% (adc: "+str(soil.read())+")") #debug

    if(moisture<=50):
      #first water the plant a little
      red.value(1)
      green.value(0)
      print(">> Soil moisture too low ("+ "%.2f" % moisture +"%), watering plants!")
      watering()
      time.sleep(5) #wait for water to absorb
      #then check
      moisture = (maxM-soil.read())*100/(maxM-minM)
      if(moisture>=50):
        goodTimes(moisture)

      else:
        continue
 
    else:
      goodTimes(moisture)
    time.sleep(1)
    
mainLoop()