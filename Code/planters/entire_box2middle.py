# --------------------------------------------------
#This script is for the thesis of 4th semester of IT-Technology, by Jacob Suurballe Petersen.
#Monitor plant health by monitoring soil moisture, sending data to node-RED and push notifications
# --------------------------------------------------

#This file is executed on every boot (including wake-boot from deepsleep)
#esp.osdebug(None)
#Inspiration and guidance taken from https://randomnerdtutorials.com/micropython-mqtt-esp32-esp8266/

#--------------
# Boot Script
#--------------

import time
from umqttsimple import MQTTClient
import ubinascii #for creating unique id
import machine #importing machine, or it runs crazy for some unknown reason
from machine import ADC, Pin #Pin for LEDs, ADC from sensor
import micropython
import network
import esp
esp.osdebug(None)
import gc
import os
gc.collect()

SSID = "networkSSID"
pw = "networkPassword"
mqtt_broker = "test.mosquitto.org"

#UNIQUE --- || MIDDLE PLANTER BOX || ---
id = ubinascii.hexlify(machine.unique_id()) #create unique ID for this ESP
topic = "plantLyfe_MiddleBox" #publishing this topic to 'var broker'

#Network connection
station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(SSID, pw)

while station.isconnected() == False:
  pass

print('Connection successful')
print(station.ifconfig()) #see connection details with IP, gateway and DNS

#----------------
# Primary Script
#----------------

#variables and pin assignments
soil = ADC(0)
maxM = 650
minM = 250
green = Pin(0, Pin.OUT) #D3
green.value(0) #default off
red = Pin(2, Pin.OUT) #D4
red.value(0) #default off
blink = Pin(14, Pin.OUT) #D5 - blink code for MQTT
blink.value(0) #default off


#MQTT connection
def MQTT_connection():
  global id, mqtt_broker, topic, client #fetches info from boot.py
  client = MQTTClient(id, mqtt_broker)
  #client.set_callback()
  client.connect()
  print("MQTT connection successful @ broker: %s" %mqtt_broker)
  return client
  
def send(moisture):
  payload = str(moisture)
  client.publish(topic, payload)
  connection()

def connection(): #blink code for MQTT connection
  if(station.isconnected()==True):
    blink.value(1)
    time.sleep(0.02)
    blink.value(0)
  else: 
    print("ERROR, NO CONNECTION")
    blink.value(0)

def goodTimes(moisture):
  while(moisture>=50):
    moist = True
    red.value(0)
    green.value(1)
    moisture = (maxM-soil.read())*100/(maxM-minM) #re-read
    print(">> Soil moisture nominal, " + "%.2f" %moisture + "%")
    send(moisture) #send 'moisture' data
    time.sleep(5)

def mainLoop():

  while True:
    moisture = (maxM-soil.read())*100/(maxM-minM)
    #print(">> moisture: " + "%.2f" % moisture + "% (adc: "+str(soil.read())+")") #debug

    if(moisture<=50):
      #first water the plant a little
      red.value(1)
      green.value(0)
      print(">> Soil moisture too low ("+ "%.2f" % moisture +"%)!")
      send(moisture) #send 'moisture' data
      moist = False
      
      while moist==False:
        moisture = (maxM-soil.read())*100/(maxM-minM)
        if(moisture<=50):
          time.sleep(5)
          print("Waiting to be watered...")
        else: moist = True
      
      #then check
      moisture = (maxM-soil.read())*100/(maxM-minM)
      payload = str(moisture)
      print("Sending payload %s to server %s" %(payload, mqtt_broker))
      if(moisture>=50):
        goodTimes(moisture)
      else:
        continue
 
    else:
      goodTimes(moisture)
    time.sleep(1)
    
def main():
  MQTT_connection()
  mainLoop()

main()