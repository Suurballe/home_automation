import time
from umqttsimple import MQTTClient
import ubinascii  # for creating unique id
import machine  # importing machine, or it runs crazy for some unknown reason'
import micropython
import network
import esp

esp.osdebug(None)
import gc
import os

mqtt_broker = "test.mosquitto.org"
id = ubinascii.hexlify(machine.unique_id())  # create unique ID for this ESP
topic = "plantLyfe_basil"  # publishing this topic to 'var broker'


# MQTT connection
def MQTT_connection():
    global id, mqtt_broker, topic, client  # fetches info from boot.py
    client = MQTTClient(id, mqtt_broker)
    # client.set_callback()
    client.connect()
    print("MQTT connection successful @ broker: %s" % mqtt_broker)
    return client


def send(moisture):
    payload = str(moisture)
    client.publish(topic, payload)
    connection()

    # then check
    moisture = (400)
    payload = str(moisture)
    print("Sending payload %s to server %s" % (payload, mqtt_broker))


MQTT_connection()
send(moisture)