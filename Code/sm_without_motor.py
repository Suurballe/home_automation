# --------------------------------------------------
#This script is for the thesis of 4th semester of IT-Technology, by Jacob Suurballe Petersen.
#Monitor plant health by monitoring soil moisture, and activating motor when below threshold.
# --------------------------------------------------

#imports already imported from boot
from machine import ADC, Pin  #ADC for moisture sensor
#import time
#from umqttsimple import MQTTClient #imports local mqtt lib

#variables and pin assignments
soil = ADC(0)
maxM = 700
minM = 200
green = Pin(0, Pin.OUT) #D3
green.value(0) #default off
red = Pin(2, Pin.OUT) #D4
red.value(0) #default off
blink = Pin(14, Pin.OUT) #D5 - blink code for MQTT
blink.value(0) #default off
moist = False

#MQTT connection
def MQTT_connection():
  global id, mqtt_broker, topic, client #fetches info from boot.py
  client = MQTTClient(id, mqtt_broker)
  #client.set_callback()
  client.connect()
  print("MQTT connection successful @ broker: %s" %mqtt_broker)
  return client
  
def send(moisture):
  payload = str(moisture)
  client.publish(topic, payload)
  connection()

def connection(): #blink code for MQTT connection
  if(station.isconnected()==True):
    blink.value(1)
    time.sleep(0.02)
    blink.value(0)
  else: 
    print("ERROR, NO CONNECTION")
    blink.value(0)

def goodTimes(moisture):
  while(moisture>=50):
    moist = True
    red.value(0)
    green.value(1)
    moisture = (maxM-soil.read())*100/(maxM-minM) #re-read
    print(">> Soil moisture nominal, " + "%.2f" %moisture + "%")
    send(moisture) #send 'moisture' data
    time.sleep(0.5)

def mainLoop():
  while True:
    moisture = (maxM-soil.read())*100/(maxM-minM)
    #print(">> moisture: " + "%.2f" % moisture + "% (adc: "+str(soil.read())+")") #debug

    if(moisture<=50):
      #first water the plant a little
      red.value(1)
      green.value(0)
      print(">> Soil moisture too low ("+ "%.2f" % moisture +"%)!")
      send(moisture) #send 'moisture' data
      moist = False
      
      while moist==False:
        moisture = (maxM-soil.read())*100/(maxM-minM)
        send(moisture)  # #####
        if(moisture<=50):
          time.sleep(2)
          print("Waiting to be watered...")
        else: moist = True
      
      #then check
      moisture = (maxM-soil.read())*100/(maxM-minM)
      payload = str(moisture)
      print("Sending payload %s to server %s" %(payload, mqtt_broker))
      if(moisture>=50):
        goodTimes(moisture)
      else:
        continue
 
    else:
      goodTimes(moisture)
    time.sleep(1)
    
def main():
  MQTT_connection()
  mainLoop()

main()