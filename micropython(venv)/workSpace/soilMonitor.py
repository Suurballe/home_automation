# --------------------------------------------------
#This script is for the thesis of 4th semester of IT-Technology, by Jacob Suurballe Petersen.
#OLED help from https://randomnerdtutorials.com/micropython-oled-display-esp32-esp8266/
# --------------------------------------------------

#imports
from machine import ADC, Pin, I2C #ADC for moisture sensor, I2C for OLED
import time
import ssd1306 #import local lib for the .96" OLED display

#variables and pin assignments
soil = ADC(0)
minM = 700
maxM = 300
"""i2c = I2C(scl=Pin(5), sda=Pin(4)) #SCL=pin D1, SDA=pin D2
oled_width = 128
oled_height = 65
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)"""
green = Pin(2, Pin.OUT)
red = Pin(0, Pin.OUT)

#main
while True:
  moisture = (maxM-soil.read())*100/(maxM-minM)
  print("moisture: " + "%.2f" % moisture + "% (adc: "+str(soil.read())+")")
  #oled.text('hello world', 0, 0)
  #oled.show()
  if(moisture<50):
    x
  elif(moisture>=50)
  time.sleep(0.5)
